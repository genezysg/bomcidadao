package entidade;


import org.junit.Test;
import junit.framework.Assert;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.DurationFieldType;


public class TestaDependente {

	@Test
	public void TestaDependente(){
		DateTime nasc = new DateTime(2000,01,01,12,0,0,0);
		Period p=new Period(nasc,DateTime.now());
		int anos=p.get(DurationFieldType.years());

		Dependente dep = new Jovem("asasdf",nasc);
		Assert.assertEquals(anos,dep.getIdade());
	}
	@Test
	public void TestaIdadeInvalidaDependenteJovem(){
		DateTime nasc = new DateTime(1950,01,01,12,0,0,0);
		Period p=new Period(nasc,DateTime.now());
		int anos=p.get(DurationFieldType.years());

		try{
		Dependente dep = new Jovem("asasdf",nasc);
		}catch (Exception e){
			Assert.assertEquals(e.getMessage(), "Tem que ter menos de 21 anos");
		}
	}
	@Test
	public void TestaDataNascimentoInvlida(){
		DateTime nasc =  DateTime.now().plusHours(2);

		try{
		Dependente dep = new Jovem("rodrigo",nasc);
		}catch (Exception e){
			Assert.assertEquals(e.getMessage(), "Data nascimento inv�lida");
		}
	}
}
