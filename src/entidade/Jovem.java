package entidade;

import java.math.BigDecimal;
import org.joda.time.DateTime;


public class Jovem extends Dependente {
	public Jovem(String nome,DateTime DataNascimento){
		super(nome,DataNascimento);
		if (getIdade()>21){
			throw new IllegalArgumentException("Tem que ter menos de 21 anos");
		}	
	}
	
	public BigDecimal calcularCusto(){
		return new BigDecimal(22.0);
	}

}
