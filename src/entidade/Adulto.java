package entidade;

import java.math.BigDecimal;
import org.joda.time.DateTime;

public class Adulto extends Dependente {
	public Adulto(String nome,DateTime DataNascimento){
		super(nome,DataNascimento);
	}
	
	public BigDecimal calcularCusto(){
		return new BigDecimal(150.0);
	}

}
