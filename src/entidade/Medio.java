package entidade;

import java.math.BigDecimal;
import org.joda.time.DateTime;

public class Medio extends Dependente {
	public Medio(String nome,DateTime DataNascimento){
		super(nome,DataNascimento);
	}
	
	public BigDecimal calcularCusto(){
		return new BigDecimal(50.0);
	}

}
