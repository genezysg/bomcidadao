package entidade;

import java.math.BigDecimal;
import org.joda.time.DateTime;

public class Senior extends Dependente {
	public Senior(String nome,DateTime DataNascimento){
		super(nome,DataNascimento);
	}
	
	public BigDecimal calcularCusto(){
		return new BigDecimal(320.0);
	}

}
